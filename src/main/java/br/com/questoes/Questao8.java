package br.com.questoes;

public class Questao8 {

	public Integer getValorC(Integer valorA, Integer valorB) {

		if(valorA == null || valorB == null) {
			return null;
		}

		StringBuilder valorC = new StringBuilder();
		int maiorTamanho = Math.max(valorA, valorB);

		for (int i = 0; i < maiorTamanho; i++) {
			if(i < valorA.toString().length()) {
				valorC.append(valorA.toString().charAt(i));
			}
			if(i < valorB.toString().length()) {
				valorC.append(valorB.toString().charAt(i));
			}
		}


		return valorC.length() > 6 ? -1: Integer.valueOf(valorC.toString());
	}

}

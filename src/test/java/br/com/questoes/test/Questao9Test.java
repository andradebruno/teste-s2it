package br.com.questoes.test;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.questoes.BinaryTree;
import br.com.questoes.Questao9;

public class Questao9Test {


	@Test
	public void testeBinaryTreeResultado() {
		Questao9 q9 = new Questao9();
		BinaryTree tree = new BinaryTree();
		tree.setValor(10);

		BinaryTree tree1 = new BinaryTree();
		tree1.setValor(20);

		BinaryTree tree2 = new BinaryTree();
		tree2.setValor(30);

		tree.setRight(tree1);
		tree.setLeft(tree2);

		assertEquals(60, q9.somaArvore(tree));


	}


	@Test
	public void testeBinaryTreeResultadoRightNull() {
		Questao9 q9 = new Questao9();
		BinaryTree tree = new BinaryTree();
		tree.setValor(10);

		BinaryTree tree1 = new BinaryTree();
		tree1.setValor(20);

		BinaryTree tree2 = new BinaryTree();
		tree2.setValor(30);

		tree.setRight(null);
		tree.setLeft(tree2);

		assertEquals(40, q9.somaArvore(tree));


	}

	@Test
	public void testeBinaryTreeResultadoLeftNull() {
		Questao9 q9 = new Questao9();
		BinaryTree tree = new BinaryTree();
		tree.setValor(10);

		BinaryTree tree1 = new BinaryTree();
		tree1.setValor(20);

		BinaryTree tree2 = new BinaryTree();
		tree2.setValor(30);

		tree.setRight(tree1);
		tree.setLeft(null);

		assertEquals(30, q9.somaArvore(tree));


	}

	@Test
	public void testeBinaryTreeResultadoAllNull() {
		Questao9 q9 = new Questao9();
		BinaryTree tree = new BinaryTree();
		tree.setValor(10);

		BinaryTree tree1 = new BinaryTree();
		tree1.setValor(20);

		BinaryTree tree2 = new BinaryTree();
		tree2.setValor(30);

		tree.setRight(null);
		tree.setLeft(null);

		assertEquals(0, q9.somaArvore(null));


	}


}

package br.com.questoes.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.questoes.Questao8;


public class Questao8Test {

	@Test
	public void valorCMenosUm(){

		assertEquals(Integer.valueOf(-1), new Questao8().getValorC(1234, 5678));

	}

	@Test
	public void valorCQtdIgual() {

		assertEquals(Integer.valueOf(142536), new Questao8().getValorC(123, 456));

	}

	@Test
	public void valorCQtdDif() {

		assertEquals(Integer.valueOf(152634), new Questao8().getValorC(1234, 56));

	}

}
